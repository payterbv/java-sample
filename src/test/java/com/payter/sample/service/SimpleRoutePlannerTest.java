package com.payter.sample.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.payter.sample.model.Boat;
import com.payter.sample.model.Harbor;
import com.payter.sample.model.HarborRepository;

@ExtendWith(MockitoExtension.class)
public class SimpleRoutePlannerTest {
	
	@InjectMocks
	private SimpleRoutePlanner planner;
	
	@Mock private HarborRepository harborRepository;

	@DisplayName("Given harbor A, when planning route for boat, returns A")
	@Test
	public void givenHarborA_WhenPlanRoute_ReturnsHarbor() throws Exception {
		
		Boat boat = mock(Boat.class);
		Harbor hA = mock(Harbor.class);
		Harbor hB = mock(Harbor.class);
		when(harborRepository.findAll()).thenReturn(List.of(hA, hB));
		
		Harbor target = planner.planRoute(boat);
		assertNotNull(target);
		assertEquals(hA, target);
	}
	
	@DisplayName("Given boat in harbor A, when planning route for boat, returns B")
	@Test
	public void givenBoatInHarborA_WhenPlanRoute_ReturnsHarbor() throws Exception {
		
		Boat boat = mock(Boat.class);
		Harbor hA = mock(Harbor.class);
		Harbor hB = mock(Harbor.class);
		when(harborRepository.findAll()).thenReturn(List.of(hA, hB));
		when(boat.getLocation()).thenReturn(hA);
		
		Harbor target = planner.planRoute(boat);
		assertNotNull(target);
		assertEquals(hB, target);
	}
}
