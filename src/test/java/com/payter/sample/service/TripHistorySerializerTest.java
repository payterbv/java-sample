package com.payter.sample.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.payter.sample.model.Boat;
import com.payter.sample.model.Harbor;
import com.payter.sample.model.Trip;

@ExtendWith(MockitoExtension.class)
public class TripHistorySerializerTest {
	
	@InjectMocks
	private TripHistorySerializer historySerializer;
	
	@Mock 
	private Boat boat;
	
	@Mock
	private Harbor h1;
	@Mock
	private Harbor h2;
	@Mock
	private Harbor h3;
	
	
	@BeforeEach
	public void setup() {
		when(boat.getName()).thenReturn("TestBoat");
		when(boat.getTrips()).thenReturn(List.of());
		
		lenient().when(h1.getId()).thenReturn(1L);
		when(h1.getName()).thenReturn("TestHarbor1");
		
		lenient().when(h2.getId()).thenReturn(2L);
		when(h2.getName()).thenReturn("TestHarbor2");
		
		lenient().when(h3.getId()).thenReturn(3L);
		when(h3.getName()).thenReturn("TestHarbor3");
	}
	
	@DisplayName("Given simple trip, When get boat history, Returns valid history")
	@Test
	public void givenSimpleTrip_WhenGetBoatHistory_ReturnsValidHistory() throws Exception {
		Trip t1 = mock(Trip.class);
		when(t1.getBoat()).thenReturn(boat);
		when(t1.getOrigin()).thenReturn(h1);
		when(t1.getDeparture()).thenReturn(LocalDate.parse("2021-01-26").atStartOfDay());
		when(t1.getDestination()).thenReturn(h2);
		when(t1.getArrival()).thenReturn(LocalDate.parse("2021-01-29").atTime(16, 24));
		
		Trip t2 = mock(Trip.class);
		when(t2.getBoat()).thenReturn(boat);
		when(t2.getOrigin()).thenReturn(h2);
		when(t2.getDestination()).thenReturn(h3);
		
		when(boat.getTrips()).thenReturn(List.of(t1, t2));
		
		String history = historySerializer.getBoatHistory(boat);
		
		assertNotNull(history);
		assertEquals("This is the trip history of the TestBoat\r\n"
				+ "It was 26 January 2021 and the 'TestBoat' left the harbor of TestHarbor1. After a long journey, the 'TestBoat' reached it's destination of TestHarbor2 on 29 January 2021.\r\n"
				+ "The 'TestBoat' is scheduled to travel to TestHarbor3 from the harbor of TestHarbor2. We eagerly await it's arrival at TestHarbor3.", history);
	}
}
