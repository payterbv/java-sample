INSERT INTO country(code, name, language) VALUES ('nl', 'Nederland', 'Nederlands');
INSERT INTO country(code, name, language) VALUES ('be', 'België', 'Vlaams/Frans');
INSERT INTO country(code, name, language) VALUES ('de', 'Deutschland', 'Deutsche');
INSERT INTO country(code, name, language) VALUES ('uk', 'United Kingdom', 'English');

INSERT INTO harbor(name, country) VALUES ('Rotterdam', 'nl');
INSERT INTO harbor(name, country) VALUES ('Hoek van Holland', 'nl');
INSERT INTO harbor(name, country) VALUES ('Antwerpen', 'be');
INSERT INTO harbor(name, country) VALUES ('Norwich', 'uk');

INSERT INTO boat(name, description, type, location) VALUES ('blue lagoon', 'a blue boat', 1, 1);
INSERT INTO boat(name, description, type, location) VALUES ('red dragon tail', 'a red canoe', 3, 2);
INSERT INTO boat(name, description, type, location) VALUES ('survivor', 'an unsinkable boat', 2, 1);

COMMIT;