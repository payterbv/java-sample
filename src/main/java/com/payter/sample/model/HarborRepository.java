package com.payter.sample.model;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface HarborRepository extends JpaRepository<Harbor, Long> {

	Optional<Harbor> findByName(@Param("name") String name);
	
	@Query("select h from Harbor h where h.country.code = :code" )
	List<Harbor> findByCountry(@Param("code") String code);
	
}
