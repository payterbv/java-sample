package com.payter.sample.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Boat {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column
	private String name;
	
	@Column
	private String description;
	
	@Column
	private BoatType type;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="location")
	@JsonBackReference
	private Harbor location;
	
	@OneToMany(mappedBy = "boat", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JsonManagedReference
	private List<Trip> trips = new ArrayList<>();
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public BoatType getType() {
		return type;
	}
	public void setType(BoatType type) {
		this.type = type;
	}
	
	public Harbor getLocation() {
		return location;
	}
	public void setLocation(Harbor location) {
		this.location = location;
	}
	
	public List<Trip> getTrips() {
		return List.copyOf(this.trips);
	}
	
	@JsonIgnore
	public Trip getLastTrip() {
		if(trips.isEmpty()) {
			return null;
		}
		return trips.get(trips.size()-1);
	}
	
	public void addTrip(Harbor destination) {
		Trip last = getLastTrip();
		if(last != null && last.getArrival() == null) {
			throw new IllegalStateException("Last trip not yet finished");
		}
		Trip nextTrip = new Trip(this);
		nextTrip.setOrigin(this.getLocation());
		nextTrip.setDestination(destination);
		trips.add(nextTrip);
	}
}
