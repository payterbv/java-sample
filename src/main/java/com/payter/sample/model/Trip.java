package com.payter.sample.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
public class Trip {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="boat")
	@JsonBackReference
	private Boat boat;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="origin")
	private Harbor origin;
	
	@Column
	private LocalDateTime departure;
	
	@Column
	private LocalDateTime arrival;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="destination")
	private Harbor destination;
	
	public Trip() {
		
	}
	
	public Trip(Boat boat) {
		this.boat = boat;
	}
	
	public Long getId() {
		return id;
	}
	
	public Boat getBoat() {
		return boat;
	}
	
	public Harbor getOrigin() {
		return origin;
	}
	public void setOrigin(Harbor origin) {
		this.origin = origin;
	}
	
	public Harbor getDestination() {
		return destination;
	}
	public void setDestination(Harbor destination) {
		this.destination = destination;
	}
	
	public LocalDateTime getDeparture() {
		return departure;
	}
	public void setDeparture(LocalDateTime departure) {
		this.departure = departure;
	}
	
	public LocalDateTime getArrival() {
		return arrival;
	}
	public void setArrival(LocalDateTime arrival) {
		this.arrival = arrival;
	}
}
