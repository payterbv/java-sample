package com.payter.sample.model;

import org.springframework.data.rest.core.config.Projection;

@Projection(name = "boatProjection", types= { Boat.class })
public interface BoatProjection {
	String getName();
	
	String getDescription();
	
	BoatType getType();
	
	Harbor getLocation();
}
