package com.payter.sample.model;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

public interface CountryRepository extends JpaRepository<Country, String> {

	Optional<Country> findByName(@Param("name") String name);
	
	List<Country> findByLanguage(@Param("lang") String language);
}
