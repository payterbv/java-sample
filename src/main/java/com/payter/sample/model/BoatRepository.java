package com.payter.sample.model;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(excerptProjection = BoatProjection.class)
public interface BoatRepository extends JpaRepository<Boat, Long> {

	@Query("select b from Boat b where b.location.id = :harbor" )
	Set<Boat> findByLocation(@Param("harbor") Long harborId);
	
	@Query("select b from Boat b where b.location.country.code = :country" )
	Set<Boat> findByLocationCountry(@Param("country") String countryCode);
}
