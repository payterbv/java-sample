package com.payter.sample.model;

public enum BoatType {

	SailBoat,
	MotorBoat,
	CargoBoat,
	RowingBoat
}
