package com.payter.sample.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Harbor {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column
	private String name;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="country")
	@JsonBackReference
	private Country country;
	
	@OneToMany(mappedBy = "location", fetch = FetchType.LAZY)
	@JsonManagedReference
	@JsonIgnore
	private Set<Boat> boats;
	
	protected Harbor() {}
	
	public Harbor(String name, Country country) {
		this.name = name;
		this.country = country;
	}
	
	public Long getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public Country getCountry() {
		return country;
	}
	public void setCountry(Country country) {
		this.country = country;
	}
	
	@JsonIgnore
	public Set<Boat> getBoats() {
		return boats;
	}
}
