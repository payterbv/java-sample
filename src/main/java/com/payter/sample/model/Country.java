package com.payter.sample.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Country {

	@Id
	private String code;
	
	@Column
	private String name;
	
	@Column
	private String language;
	
	@OneToMany(mappedBy = "country", fetch = FetchType.LAZY)
	@JsonManagedReference
	private Set<Harbor> harbours = new HashSet<>();
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	
	public Set<Harbor> getHarbours() {
		return harbours;
	}
}
