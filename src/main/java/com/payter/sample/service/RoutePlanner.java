package com.payter.sample.service;

import com.payter.sample.model.Boat;
import com.payter.sample.model.Harbor;

public interface RoutePlanner {

	Harbor planRoute(Boat boat);
}
