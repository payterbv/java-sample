package com.payter.sample.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.payter.sample.model.Boat;
import com.payter.sample.model.Harbor;
import com.payter.sample.model.HarborRepository;
import com.payter.sample.model.Trip;

@Service
public class SimpleRoutePlanner implements RoutePlanner {

	@Autowired
	private HarborRepository harborRepository;
	
	@Override
	public Harbor planRoute(Boat boat) {
		Trip trip = boat.getLastTrip();
		if(trip != null && trip.getArrival() == null) {
			throw new IllegalStateException("Boat is currently already traveling");
		}
		
		//we just go anywhere
		for(Harbor harbor : harborRepository.findAll()) {
			if(!harbor.equals(boat.getLocation())) {
				return harbor;
			}
		}
		
		return null;
	}

}
