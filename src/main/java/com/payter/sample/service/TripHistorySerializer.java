package com.payter.sample.service;

import java.time.format.DateTimeFormatter;

import org.springframework.stereotype.Service;

import com.payter.sample.model.Boat;
import com.payter.sample.model.Trip;

@Service
public class TripHistorySerializer {
	
	private static final DateTimeFormatter format = DateTimeFormatter.ofPattern("d MMMM YYYY");
	
	public TripHistorySerializer() {}

	public String getBoatHistory(Boat boat) {
		StringBuffer builder = new StringBuffer("This is the trip history of the ").append(boat.getName());
		for(Trip trip : boat.getTrips()) {
			builder.append(System.lineSeparator());
			serializeTrip(trip, builder);
		}
		return builder.toString();
	}
	
	private void serializeTrip(Trip trip, StringBuffer buffer) {
		if(trip.getDeparture() != null) {
			buffer.append("It was ").append(format.format(trip.getDeparture())).append(" and the '")
				.append(trip.getBoat().getName()).append("' left the harbor of ").append(trip.getOrigin().getName());
		} else {
			buffer.append("The '").append(trip.getBoat().getName()).append("' is scheduled to travel to ")
				.append(trip.getDestination().getName()).append(" from the harbor of ").append(trip.getOrigin().getName());
		}
		buffer.append(". ");
		if(trip.getArrival() != null) {
			buffer.append("After a long journey, the '").append(trip.getBoat().getName()).append("' reached it's destination of ")
					.append(trip.getDestination().getName()).append(" on ").append(format.format(trip.getArrival()));
		} else {
			buffer.append("We eagerly await it's arrival at ").append(trip.getDestination().getName());
		}
		buffer.append(".");
	}
}
