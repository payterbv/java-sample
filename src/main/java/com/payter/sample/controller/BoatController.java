package com.payter.sample.controller;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.payter.sample.model.Boat;
import com.payter.sample.model.BoatRepository;
import com.payter.sample.model.Harbor;
import com.payter.sample.model.Trip;
import com.payter.sample.service.RoutePlanner;
import com.payter.sample.service.TripHistorySerializer;

@RepositoryRestController
public class BoatController {

	@Autowired
	private BoatRepository repository;
	
	@Autowired
	@Qualifier("simpleRoutePlanner")
	private RoutePlanner planner;
	
	@Autowired
	@Qualifier("tripHistorySerializer")
	private TripHistorySerializer serializer;
	
	@RequestMapping(path = "boats/_hello", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> helloWorld() {
		return ResponseEntity.ok("Hello World!");
	}
	
	@RequestMapping(path = "boats/_search/location", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<?> findByLocation(@RequestBody Harbor harbor, PersistentEntityResourceAssembler entityAssembler) {
		Set<Boat> boats = repository.findByLocation(harbor.getId());
		
		return ResponseEntity.ok(entityAssembler.toCollectionModel(boats));
	}
	
	@RequestMapping(path = "boats/{id}/route", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<?> routeBoat(@PathVariable Long id, PersistentEntityResourceAssembler entityAssembler) {
		Optional<Boat> b = repository.findById(id);
		
		if(b.isPresent()) {
			Boat boat = b.get();
			try {
				Harbor harbor = planner.planRoute(boat);
				boat.addTrip(harbor);
				repository.save(boat);
				return ResponseEntity.ok(entityAssembler.toFullResource(boat.getLastTrip()));
			} catch(IllegalStateException e) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
			}
		}
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
	}
	
	@RequestMapping(path = "boats/{id}/departed", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<?> boatDeparted(@PathVariable Long id, LocalDateTime date)  {
		Optional<Boat> b = repository.findById(id);
		
		if(b.isPresent()) {
			Boat boat = b.get();
			Trip trip = boat.getLastTrip();
			if(trip == null || trip.getDeparture() != null) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
			}
			
			if(date == null) {
				date = LocalDateTime.now();
			}
			
			trip.setDeparture(date);
			repository.save(boat);
			
			return ResponseEntity.ok(date);
		}
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
	}
	
	@RequestMapping(path = "boats/{id}/arrived", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<?> boatArrived(@PathVariable Long id, LocalDateTime date)  {
		Optional<Boat> b = repository.findById(id);
		
		if(b.isPresent()) {
			Boat boat = b.get();
			Trip trip = boat.getLastTrip();
			if(trip == null || trip.getDeparture() == null || trip.getArrival() != null) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("no trip scheduled or not yet departed");
			}

			if(date == null) {
				date = LocalDateTime.now();
			}
			
			trip.setArrival(date);
			boat.setLocation(trip.getDestination());
			repository.save(boat);
			
			return ResponseEntity.ok(date);
		}
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
	}
		
	@RequestMapping(path = "boats/{id}/history", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<?> boatHistory(@PathVariable Long id)  {
		Optional<Boat> b = repository.findById(id);
		
		if(b.isPresent()) {
			Boat boat = b.get();
			String history = this.serializer.getBoatHistory(boat);
			return ResponseEntity.ok(history);
		}
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
	}
}
